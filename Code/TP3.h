#ifndef TP3_H
#define TP3_H


/**
 * @brief Retrourne un nombre pseudo aléatoire entre [a;b]
 * 
 * @param a a
 * @param b b
 * @return double nombre pseudo aléatoire
 */
double uniform(double a, double b);


/**
 * @brief Retrourne un nombre pseudo aléatoire entre [a;b[]
 * 
 * @param a a
 * @param b b
 * @return double Nombre pseudo aléatoire
 */
double uniform_exclusive(double a, double b);


/**
 * @brief Simule la valeur de PI avec Monte Carlo
 * 
 * @param numberOfPoints Nombre de point à tirer
 * @return double Valeur de PI simulé
 */
double simulationPiMC(long numberOfPoints);

/**
 * @brief Simule N fois la valeur de PI avec Monte Carlo
 * 
 * @param numberOfPoints Nombre de point à tirer
 * @param experiments Nombre de tirage à faire
 * @return double Moyenne des tirages
 */
double simulationPisMC(long numberOfPoints, int experiments);


/**
 * @brief Simule N fois la valeur de PI avec Monte Carlo
 * 
 * @param numberOfPoints Nombre de point à tirer
 * @param experiments Nombre de tirage à faire
 * @return double * Les tirages
 */
double * simulationPisMCArray(long numberOfPoints, int experiments);


/**
 * @brief Calcul la variance estimée d'un résultat
 * 
 * @param results Les résultats
 * @param numberOfResults Le nombre de résultats
 * @param mean La moyenne théorique
 * @return double Variance estimée
 */
double confidenceRadius(double * results, int numberOfResults, double mean);



/**
 * @brief Calcul le rayon de confiance d'un resultat
 * 
 * @param results Les résultats
 * @param numberOfResults Le nombre de résultats
 * @param mean La moyenne théorique
 * @return double Rayon de confiance
 */
double confidenceRadius(double * results, int numberOfResults, double mean);



#endif