#include "tests.h"

#include "TP3.h"
#include "color.h"

#include "mt19937ar.h"
#include <float.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

void testPiMC(void) {
	time_t start_t, end_t;

	time(&start_t);
	printf("With 1000000000 points : PI = %lf\n", simulationPiMC(1000));
	time(&end_t);
	printf("In %lf s\n", difftime(end_t, start_t));
	// 38 secondes sans optimisation
	// 12 secondes avec -02
	// 11 secondes avec -03
}

void testPisMC(void) {
	time_t start_t, end_t;
	double simulatedPi;

	time(&start_t);
	simulatedPi = simulationPisMC(1000000, 30);
	time(&end_t);

	printf("With 1 000 000 points and 300 experiments : PI = %lf\n", simulatedPi);
	printf("Relative error = %lf\n", (simulatedPi - M_PI) / M_PI);
	printf("In %lf s\n", difftime(end_t, start_t));
}

void testPisMCConfidenceRadius(void) {
	double *results;
	double confidence;
	int numberOfResults = 30;
	long numberOfPoints = 10000000;

	results = simulationPisMCArray(numberOfPoints, numberOfResults);
	confidence = confidenceRadius(results, numberOfResults, M_PI);

	printf("For %ld points and %d experiments :\n",numberOfPoints, numberOfResults );
	printf("\tConfidence Interval at 95%%\t: "GRE"[ %.17g ; %.17g ]\n"RESET, M_PI - confidence, M_PI + confidence);
	printf("\tConfidence Diameter\t\t: "GRE"%.17g\n"RESET, 2 * confidence);

	free(results);
}