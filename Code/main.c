#include "TP3.h"
#include "tests.h"

#include "mt19937ar.h"



/**
 * @brief Point d'entrée
 * 
 * @param argc Nombre d'arguments
 * @param argv Arguments
 * @return int Code d'erreur
 */
int main(int argc, char const *argv[]) {
	(void)argc;
	(void)argv;

	init_default();

	testPiMC();
	//testPisMC();
	//testPisMCConfidenceRadius();
	
	return 0;
}
