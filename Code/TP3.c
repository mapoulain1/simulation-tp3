#include "TP3.h"
#include "color.h"
#include "tests.h"


#include "mt19937ar.h"
#include <float.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/**
 * @brief Retrourne un nombre pseudo aléatoire entre [a;b]
 * 
 * @param a a
 * @param b b
 * @return double nombre pseudo aléatoire
 */
double uniform(double a, double b) {
	return genrand_real1() * (b - a) + a;
}


/**
 * @brief Retrourne un nombre pseudo aléatoire entre [a;b[]
 * 
 * @param a a
 * @param b b
 * @return double Nombre pseudo aléatoire
 */
double uniform_exclusive(double a, double b) {
	return genrand_real2() * (b - a) + a;
}

/**
 * @brief Simule la valeur de PI avec Monte Carlo
 * 
 * @param numberOfPoints Nombre de point à tirer
 * @return double Valeur de PI simulé
 */
double simulationPiMC(long numberOfPoints){
	long pointsInsideDisk = 0;
	double x;
	double y;

	for (long i = 0; i < numberOfPoints; i++)
	{
		x = genrand_real1();
		y = genrand_real1();
		if(x * x + y * y < 1){
			pointsInsideDisk++;
		}
	}
	return 4.0 * ((double)pointsInsideDisk / numberOfPoints);
}



/**
 * @brief Simule N fois la valeur de PI avec Monte Carlo
 * 
 * @param numberOfPoints Nombre de point à tirer
 * @param experiments Nombre de tirage à faire
 * @return double Moyenne des tirages
 */
double simulationPisMC(long numberOfPoints, int experiments){
	double sum = 0;
	for (int i = 0; i < experiments; i++)
	{
		sum += simulationPiMC(numberOfPoints);
	}
	return sum / experiments;
}



/**
 * @brief Simule N fois la valeur de PI avec Monte Carlo
 * 
 * @param numberOfPoints Nombre de point à tirer
 * @param experiments Nombre de tirage à faire
 * @return double * Les tirages
 */
double * simulationPisMCArray(long numberOfPoints, int experiments){
	double * results = malloc(experiments * sizeof(double));
	if(!results){
		fprintf(stderr, "Malloc error\n");
		return NULL;
	}

	for (int i = 0; i < experiments; i++)
	{
		results[i] = simulationPiMC(numberOfPoints);
		printf("%.17lf\n", results[i]);
	}
	return results;
}

/**
 * @brief Calcul la variance estimée d'un résultat
 * 
 * @param results Les résultats
 * @param numberOfResults Le nombre de résultats
 * @param mean La moyenne théorique
 * @return double Variance estimée
 */
double estimatedVariance(double * results, int numberOfResults, double mean){
	double sum = 0;
	for (int i = 0; i < numberOfResults; i++)
	{
		sum += (results[i] - mean)*(results[i] - mean);
	}
	return sum / (numberOfResults - 1);
}


/**
 * @brief Calcul le rayon de confiance d'un resultat
 * 
 * @param results Les résultats
 * @param numberOfResults Le nombre de résultats
 * @param mean La moyenne théorique
 * @return double Rayon de confiance
 */
double confidenceRadius(double * results, int numberOfResults, double mean){
	double student[] = {12.706,4.3027,3.1824,2.7765,2.5706,2.4469,2.3646,2.306,2.2622,2.2281,2.201,2.1788,2.1604,2.1448,2.1315,2.1199,2.1098,2.1009,2.093,2.086,2.0796,2.0739,2.0687,2.0639,2.0595,2.0555,2.0518,2.0484,2.0452,2.0423,2.0395,2.0369,2.0345,2.0322,2.0301,2.0281,2.0262,2.0244,2.0227,2.0211,2.0195,2.0181,2.0167,2.0154,2.0141,2.0129,2.0117,2.0106,2.0096,2.0086,1.96};
	//student[50] pour +infini

	double studentSelected = student[numberOfResults > 50 ? 50 : numberOfResults];
	double variance = estimatedVariance(results, numberOfResults, mean);
	return studentSelected * sqrt((variance*variance)/numberOfResults);


}